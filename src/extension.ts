import vscode, { WorkspaceFolder } from 'vscode'

import Command from './command'
import config from './config'
import JustFormatter from './formatter'
import Runner from './runner'
import JustTaskProvider from './taskProvider'

const installUri = vscode.Uri.parse('https://just.systems/man/en/chapter_2.html')

export async function activate(context: vscode.ExtensionContext) {
	const output = vscode.window.createOutputChannel('just', { log: true })
	context.subscriptions.push(output)
	const runner = new Runner(output)

	if (!(await checkExecutable(runner))) return
	const formatter = new JustFormatter(runner)
	const taskProvider = new JustTaskProvider(runner)
	context.subscriptions.push(
		vscode.commands.registerCommand(Command.evaluate, async () => {
			const workspace = getActiveWorkspace()
			const variables = runner.variables(workspace)
			const variable = await vscode.window.showQuickPick(variables, {
				title: 'Choose a variable to evaluate',
			})
			if (!variable) return
			const result = await runner.evaluate(variable, workspace)
			await vscode.window.showInformationMessage(result)
		}),
		vscode.commands.registerCommand(Command.command, async () => {
			const command = await vscode.window.showInputBox({
				title: 'just: Run command',
				prompt: 'Enter a command to run',
			})
			if (!command) return
			const workspace = getActiveWorkspace()
			const task = taskProvider.commandTask(command, workspace)
			await vscode.tasks.executeTask(task)
		}),
		vscode.languages.registerDocumentFormattingEditProvider(
			JustFormatter.Selector,
			formatter,
		),
		vscode.tasks.registerTaskProvider(JustTaskProvider.Type, taskProvider),
	)
}

export function deactivate() {
	// nothing
}

async function checkExecutable(runner: Runner): Promise<boolean> {
	const workspace = getActiveWorkspace()
	if (await runner.isAvailable(workspace)) return true
	const options = ['Configure', 'Install', 'Ignore']
	const choice = await vscode.window.showErrorMessage(
		'Could not run just, is it installed?',
		...options,
	)
	switch (choice) {
		case 'Configure':
			await config.open('executablePath')
			return true
		case 'Install':
			await vscode.env.openExternal(installUri)
			return true
	}
	return false
}

function getActiveWorkspace(): WorkspaceFolder | undefined {
	const uri = vscode.window.activeTextEditor?.document.uri
	const isFile = uri && uri.fsPath && uri.scheme === 'file'
	if (isFile) return vscode.workspace.getWorkspaceFolder(uri)
	return vscode.workspace.workspaceFolders?.[0]
}
