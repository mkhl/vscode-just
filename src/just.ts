export interface File {
	assignments: Record<string, object>
	recipes: Record<string, Recipe>
}

export interface Recipe {
	name: string
	doc?: string
	parameters: Parameter[]
	private: boolean
}

export interface Parameter {
	name: string
	kind: 'singular' | 'plus' | 'star'
	// default?: string[]
	// export: boolean
}
