import cp from 'child_process'
import { promisify } from 'util'

import { LogOutputChannel, WorkspaceFolder } from 'vscode'

import config from './config'
import * as just from './just'

const execFile = promisify(cp.execFile)

export default class Runner {
	constructor(private output: LogOutputChannel) {}

	async evaluate(variable: string, workspace?: WorkspaceFolder): Promise<string> {
		const output = await this.run(['--evaluate', variable], workspace)
		return output.trim()
	}

	async format(path: string, workspace?: WorkspaceFolder): Promise<string | undefined> {
		return await this.run(['--dump', '--justfile', path], workspace)
	}

	async recipes(workspace?: WorkspaceFolder): Promise<just.Recipe[]> {
		const output = await this.run(['--dump', '--dump-format', 'json'], workspace)
		const json = JSON.parse(output) as just.File
		return Object.values(json.recipes)
	}

	async variables(workspace?: WorkspaceFolder): Promise<string[]> {
		const output = await this.run(['--dump', '--dump-format', 'json'], workspace)
		const json = JSON.parse(output) as just.File
		return Object.keys(json.assignments)
	}

	async isAvailable(workspace?: WorkspaceFolder): Promise<boolean> {
		try {
			await this.run(['--version'], workspace)
			return true
		} catch (_) {
			return false
		}
	}

	private async run(
		args: string[],
		workspace?: WorkspaceFolder,
		env: NodeJS.ProcessEnv = {},
	): Promise<string> {
		this.output.info(workspace?.name ?? '[no workspace]', 'just', ...args)
		const cwd = workspace?.uri.fsPath ?? process.cwd()
		const cmd = config.resolve(workspace).executablePath
		const options: cp.ExecOptionsWithStringEncoding = {
			encoding: 'utf8',
			cwd,
			env: {
				...process.env,
				['TERM']: 'dumb',
				...env,
			},
		}
		try {
			const { stdout, stderr } = await execFile(cmd, args, options)
			if (stderr) {
				this.output.warn(stderr)
			}
			return stdout
		} catch (e) {
			if (typeof e === 'string' || e instanceof Error) {
				this.output.error(e)
			}
			throw e
		}
	}
}
