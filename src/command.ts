const enum Command {
	evaluate = 'mkhl-just.evaluate',
	command = 'mkhl-just.command',
}

export default Command
