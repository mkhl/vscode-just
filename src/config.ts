import settings from '@mkhl/vscode-settings'

export default settings('mkhl-just', {
	executablePath: 'just',
	executableArgs: [] as string[],
})
