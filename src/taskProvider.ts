import vscode, {
	Disposable,
	ProcessExecution,
	ProviderResult,
	RelativePattern,
	Task,
	TaskDefinition,
	TaskProvider,
	TaskScope,
	WorkspaceFolder,
} from 'vscode'

import * as shlex from 'shlex'

import config from './config'
import Runner from './runner'

export default class JustTaskProvider implements TaskProvider, Disposable {
	static readonly Type = 'just'

	private workspaces = new Map<string, JustTaskWorkspace>()
	private disposable: Disposable

	constructor(private runner: Runner) {
		this.disposable = this.watchWorkspaces()
	}

	private watchWorkspaces() {
		const folders = vscode.workspace.workspaceFolders ?? []
		for (const folder of folders) {
			this.addWorkspace(folder)
		}
		return vscode.workspace.onDidChangeWorkspaceFolders((e) => {
			for (const folder of e.removed) {
				this.removeWorkspace(folder)
			}
			for (const folder of e.added) {
				this.addWorkspace(folder)
			}
		})
	}

	private addWorkspace(folder: WorkspaceFolder) {
		const key = folder.uri.fsPath
		if (!key) return
		if (this.workspaces.has(key)) return
		this.workspaces.set(key, new JustTaskWorkspace(folder, this.runner))
	}

	private removeWorkspace(folder: WorkspaceFolder) {
		const key = folder.uri.fsPath
		this.workspaces.get(key)?.dispose()
		this.workspaces.delete(key)
	}

	dispose() {
		this.disposable.dispose()
		for (const folder of this.workspaces.values()) {
			folder.dispose()
		}
	}

	async provideTasks(): Promise<Task[]> {
		const workspaces = Array.from(this.workspaces.values())
		const promises = workspaces.map((workspace) => workspace.provideTasks())
		const tasks = await Promise.all(promises)
		return tasks.flat()
	}

	resolveTask(task: Task): ProviderResult<Task> {
		if (!isJustTaskDefinition(task.definition) || typeof task.scope !== 'object') return
		task.execution = execute(task.scope, task.definition)
		return task
	}

	commandTask(command: string, workspace?: WorkspaceFolder): Task {
		const definition = { type: JustTaskProvider.Type }
		const execution = executeCommand(command, workspace)
		const scope = workspace ?? TaskScope.Workspace
		const name = `just --command ${command}`
		return new Task(definition, scope, name, config.section, execution)
	}
}

class JustTaskWorkspace implements Disposable {
	private disposable: Disposable
	private loading?: Thenable<Task[]>

	constructor(
		private workspace: WorkspaceFolder,
		private runner: Runner,
	) {
		this.disposable = Disposable.from(
			this.watchConfiguration(workspace),
			this.watchFileSystem(workspace, '.justfile'),
			this.watchFileSystem(workspace, 'justfile'),
			this.watchFileSystem(workspace, 'Justfile'),
			this.watchFileSystem(workspace, '**/*.just'),
		)
	}

	private watchConfiguration(workspace: WorkspaceFolder) {
		return vscode.workspace.onDidChangeConfiguration((e) => {
			if (e.affectsConfiguration(config.section, workspace)) {
				this.reload()
			}
		})
	}

	private watchFileSystem(workspace: WorkspaceFolder, pattern: string): Disposable {
		const relative = new RelativePattern(workspace, pattern)
		const watcher = vscode.workspace.createFileSystemWatcher(relative)
		watcher.onDidChange(() => this.reload())
		watcher.onDidCreate(() => this.reload())
		watcher.onDidDelete(() => this.reload())
		return watcher
	}

	private reload() {
		this.loading = undefined
	}

	dispose() {
		this.disposable.dispose()
	}

	async provideTasks(): Promise<Task[]> {
		if (!this.workspace.uri.fsPath) return []
		if (!this.loading) {
			this.loading = this.loadTasks()
		}
		return this.loading
	}

	private async loadTasks(): Promise<Task[]> {
		const recipes = await this.runner.recipes(this.workspace)
		const type = JustTaskProvider.Type
		return recipes
			.filter((recipe) => !recipe.private)
			.map((recipe) => {
				const definition = { type, name: recipe.name }
				const execution = execute(this.workspace, definition)
				return new Task(
					definition,
					this.workspace,
					recipe.name,
					config.section,
					execution,
				)
			})
	}
}

function execute(
	workspace: WorkspaceFolder,
	task: JustTaskDefinition,
): ProcessExecution {
	const { executablePath, executableArgs } = config.resolve(workspace)
	const fileArgs = task.file ? ['--justfile', task.file] : []
	const taskArgs = task.arguments ?? []
	return new ProcessExecution(executablePath, [
		...executableArgs,
		...fileArgs,
		task.name,
		...taskArgs,
	])
}

function executeCommand(
	command: string,
	workspace?: WorkspaceFolder,
): ProcessExecution {
	const { executablePath, executableArgs } = config.resolve(workspace)
	const commandArgs = shlex.split(command)
	return new ProcessExecution(executablePath, [
		...executableArgs,
		'--command',
		...commandArgs,
	])
}

interface JustTaskDefinition extends TaskDefinition {
	name: string
	arguments?: string[]
	file?: string
}

function isJustTaskDefinition(def: TaskDefinition): def is JustTaskDefinition {
	return def.type === JustTaskProvider.Type && 'name' in def
}
