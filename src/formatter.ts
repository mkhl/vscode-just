import fs from 'fs/promises'
import os from 'os'
import path from 'path'

import vscode, {
	DocumentFormattingEditProvider,
	DocumentSelector,
	Range,
	TextDocument,
	TextEdit,
	WorkspaceFolder,
} from 'vscode'

import Runner from './runner'

const everything = new Range(0, 0, Number.MAX_SAFE_INTEGER, Number.MAX_SAFE_INTEGER)

export default class JustFormatter implements DocumentFormattingEditProvider {
	static readonly Selector: DocumentSelector = { language: 'just' }

	constructor(private runner: Runner) {}

	async provideDocumentFormattingEdits(
		document: TextDocument,
		// _options: FormattingOptions,
		// _token: CancellationToken,
	): Promise<TextEdit[] | undefined> {
		const workspace = vscode.workspace.getWorkspaceFolder(document.uri)
		if (!document.isDirty && !document.isUntitled && document.uri.fsPath) {
			const file = document.uri.fsPath
			return this.doProvideDocumentFormattingEdits(file, document, workspace)
		}
		const tmpdir = await fs.mkdtemp(path.join(os.tmpdir(), 'mkhl-just'))
		const file = path.join(tmpdir, 'justfile')
		await fs.writeFile(file, document.getText())
		try {
			return this.doProvideDocumentFormattingEdits(file, document, workspace)
		} finally {
			await fs.rm(tmpdir, { recursive: true })
		}
	}

	private async doProvideDocumentFormattingEdits(
		path: string,
		document: TextDocument,
		workspace?: WorkspaceFolder,
		// _options: FormattingOptions,
		// _token: CancellationToken,
	): Promise<TextEdit[] | undefined> {
		const total = document.validateRange(everything)
		const formatted = await this.runner.format(path, workspace)
		if (!formatted) return
		return [new TextEdit(total, formatted)]
	}
}
