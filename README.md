# just Command Runner for Visual Studio Code

[just] is a command runner,
a handy way to save and run project-specific commands.

This extension adds just support
to Visual Studio Code and its derivatives
by letting you:

* run just recipes (as a [task provider][vscode-task-provider])
* format justfiles (as a [document formatter][vscode-formatter])
* run commands with just
* evaluate variables from justfiles

It does not support:

* creating justfiles
* editing justfiles
* global justfiles


## Features

Run recipes with the "Tasks: Run Task" command.
The extension does not provide a [problem matcher][vscode-problem-matcher]
because output is handled by the commands just runs, not just itself.

Format justfiles with the "Format Document" command.

Run arbitrary commands in the context of just with the "just: Run command" command.

Evaluate variables from your justfiles with the "just: Evaluate variables" command.


## Requirements

This extension requires [just] to be installed.


## Extension Settings

This extension contributes the following settings:

* `just.executablePath`: Path to or name of the just executable.
* `just.executableArgs`: Additional arguments passed to the just executable.


## Known Issues

just formats justfiles according to its own rules,
disregarding any [formatting options][vscode-formatting-options] you might have set.

This list is incomplete, you can help by expanding it.


## What's Up With That Icon?

It's a robot, but it's gift-wrapped!
Found on [Emoji Kitchen][emoji-kitchen].


## What's With That Name?

Microsoft requires that all new extensions on their marketplace
have a unique name and display name
*across all publishers*.
While having no way to transfer names between publishers.

So the only way to publish an extension
*without squatting the name*
is to include your publisher name in the extension name.


[just]: https://just.systems/
[make]: https://pubs.opengroup.org/onlinepubs/9699919799/utilities/make.html#top
[emoji-kitchen]: https://emoji.supply/kitchen/
[vscode-formatter]: https://code.visualstudio.com/api/references/vscode-api#DocumentFormattingEditProvider
[vscode-formatting-options]: https://code.visualstudio.com/api/references/vscode-api#FormattingOptions
[vscode-problem-matcher]: https://code.visualstudio.com/api/references/vscode-api#Task
[vscode-task-provider]: https://code.visualstudio.com/api/extension-guides/task-provider
