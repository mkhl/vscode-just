# Change Log

All notable changes to this project will be documented in this file.

Check [Keep a Changelog](http://keepachangelog.com/) for recommendations on how to structure this file.

## [Unreleased]

## [0.0.3] - 2024-05-28
- Rename extension *more*

## [0.0.2] - 2024-05-27
- Rename extension

## [0.0.1] - 2024-05-27
- Initial release

[Unreleased]: https://codeberg.org/mkhl/vscode-just/compare/v0.0.3...HEAD
[0.0.3]: https://codeberg.org/mkhl/vscode-just/compare/v0.0.2...v0.0.3
[0.0.2]: https://codeberg.org/mkhl/vscode-just/compare/v0.0.1...v0.0.2
[0.0.1]: https://codeberg.org/mkhl/vscode-just/tag/v0.0.1
